FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/tarifa-*.jar

WORKDIR /appagent
COPY /appagent/ ./

WORKDIR /app
COPY ${JAR_FILE} ./api-tarifa.jar

CMD ["java","-javaagent:/appagent/javaagent.jar","-jar","/app/api-tarifa.jar"]
