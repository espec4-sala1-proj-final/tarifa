package br.com.itau.tarifas.tarifa.models;

import javax.persistence.*;

@Entity
@Table
public class Tarifa {

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String nome;

    private Integer produtoId;

    private Integer contaContabilId;

    public Tarifa() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(Integer produtoId) {
        this.produtoId = produtoId;
    }

    public Integer getContaContabilId() {
        return contaContabilId;
    }

    public void setContaContabilId(Integer contaContabilId) {
        this.contaContabilId = contaContabilId;
    }

    @Override
    public String toString() {
        return "Tarifa{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", produtoId=" + produtoId +
                ", contaContabilId=" + contaContabilId +
                '}';
    }
}
