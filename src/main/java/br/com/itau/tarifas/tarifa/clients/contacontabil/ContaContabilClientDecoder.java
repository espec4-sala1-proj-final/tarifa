package br.com.itau.tarifas.tarifa.clients.contacontabil;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ContaContabilClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ContaContabilNotFoundException();
        }
        return errorDecoder.decode(s, response);

    }
}
