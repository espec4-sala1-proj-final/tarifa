package br.com.itau.tarifas.tarifa.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class VigenciaInvalidaException extends RuntimeException {

    public VigenciaInvalidaException(String var1) {super(var1);}
}
