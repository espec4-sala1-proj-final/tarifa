package br.com.itau.tarifas.tarifa.clients.produto;

public class ProdutoClientFallback implements ClientProduto {

    @Override
    public Produto buscarPorId(Integer Id) {
        throw new ProdutoNotFoundException();
    }
}
