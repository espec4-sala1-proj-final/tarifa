package br.com.itau.tarifas.tarifa.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Vigência não cadastrada para a tarifa consultada")
public class VigenciaNotFoundException extends RuntimeException {
}
