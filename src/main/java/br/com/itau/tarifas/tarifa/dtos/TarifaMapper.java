package br.com.itau.tarifas.tarifa.dtos;

import br.com.itau.tarifas.tarifa.dtos.request.TarifaRequest;
import br.com.itau.tarifas.tarifa.dtos.response.TarifaResponse;
import br.com.itau.tarifas.tarifa.models.Tarifa;
import org.springframework.stereotype.Component;

@Component
public class TarifaMapper {

    public Tarifa toTarifa(TarifaRequest tarifaRequest){
        Tarifa tarifa = new Tarifa();
        tarifa.setNome(tarifaRequest.getNome());
        tarifa.setProdutoId(tarifaRequest.getProdutoId());
        tarifa.setContaContabilId(tarifaRequest.getContaContabilId());
        return tarifa;
    }

    public TarifaResponse toTarifaResponse(Tarifa tarifa ) {
        TarifaResponse tarifaResponse = new TarifaResponse();
        tarifaResponse.setId(tarifa.getId());
        tarifaResponse.setNome(tarifa.getNome());
        tarifaResponse.setProdutoId(tarifa.getProdutoId());
        tarifaResponse.setContaContabilId(tarifa.getContaContabilId());
        return tarifaResponse;
    }

}