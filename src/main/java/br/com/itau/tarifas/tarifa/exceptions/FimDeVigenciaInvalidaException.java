package br.com.itau.tarifas.tarifa.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class FimDeVigenciaInvalidaException extends RuntimeException {

    public FimDeVigenciaInvalidaException(String var1) {super(var1);}
}
