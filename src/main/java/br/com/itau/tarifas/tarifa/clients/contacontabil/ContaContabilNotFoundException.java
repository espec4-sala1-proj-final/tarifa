package br.com.itau.tarifas.tarifa.clients.contacontabil;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Conta Contabil não encontrada")
public class ContaContabilNotFoundException extends RuntimeException {
}

