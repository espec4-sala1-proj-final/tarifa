package br.com.itau.tarifas.tarifa.clients.contacontabil;

public class ContaContabilClientFallback implements ClientContaContabil {

    @Override
    public ContaContabil pesquisarPorId(int id) {
        throw new ContaContabilNotFoundException();
    }
}
