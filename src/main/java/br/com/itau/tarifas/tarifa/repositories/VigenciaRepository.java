package br.com.itau.tarifas.tarifa.repositories;

import br.com.itau.tarifas.tarifa.models.Vigencia;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface VigenciaRepository extends CrudRepository<Vigencia, Integer> {

    Iterable<Vigencia> findByTarifaId(int tarifaId);
    Optional<Vigencia> findByIdAndTarifaId(int id, int tarifaId);
}
