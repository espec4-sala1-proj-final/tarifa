package br.com.itau.tarifas.tarifa.dtos.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class CriarVigenciaDTO {

    @NotNull(message = "Data inicial de vigência não pode ser nula")
    private LocalDate inicioVigencia;

    @NotNull(message = "Data fim de vigência não pode ser nula")
    private LocalDate fimVigencia;

    @NotNull(message = "Valor mínimo não pode ser nulo")
    @Digits(integer = 6, fraction = 2, message = "Valor aplicado fora do padrão")
    private double valorMinimo;

    @NotNull(message = "Valor máximo não pode ser nulo")
    @Digits(integer = 6, fraction = 2, message = "Valor aplicado fora do padrão")
    private double valorMaximo;

    public CriarVigenciaDTO() {
    }

    public LocalDate getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(LocalDate inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public LocalDate getFimVigencia() {
        return fimVigencia;
    }

    public void setFimVigencia(LocalDate fimVigencia) {
        this.fimVigencia = fimVigencia;
    }

    public double getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(double valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public double getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(double valorMaximo) {
        this.valorMaximo = valorMaximo;
    }
}
