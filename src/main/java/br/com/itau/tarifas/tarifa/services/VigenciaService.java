package br.com.itau.tarifas.tarifa.services;

import br.com.itau.tarifas.tarifa.exceptions.FimDeVigenciaInvalidaException;
import br.com.itau.tarifas.tarifa.exceptions.VigenciaNotFoundException;
import br.com.itau.tarifas.tarifa.exceptions.VigenciaInvalidaException;
import br.com.itau.tarifas.tarifa.models.Vigencia;
import br.com.itau.tarifas.tarifa.repositories.VigenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class VigenciaService {

    @Autowired
    private VigenciaRepository vigenciaRepository;

    public Iterable<Vigencia> consultarVigenciasPorTarifaId(int tarifaId) {
        return vigenciaRepository.findByTarifaId(tarifaId);
    }

    public Vigencia criarVigencia(Vigencia vigencia) {
        LocalDate dataAtual = LocalDate.now();
        List<Vigencia> vigenciaList = (List) consultarVigenciasPorTarifaId(vigencia.getTarifa().getId());
        if(verificarSobreposicao(vigenciaList, vigencia)){
            throw new VigenciaInvalidaException("Período de vigência sobrepõe intervalo já cadastrado para a tarifa solicitada");
        }
        if(vigencia.getInicioVigencia().isBefore(dataAtual)) {
            throw new VigenciaInvalidaException("Não é possível criar vigência com data de início menor que data atual");
        }
        Vigencia vigenciaDB = vigenciaRepository.save(vigencia);
        System.out.println("Vigência cadastrada com sucesso: " + vigenciaDB.toString());
        return vigenciaDB;
    }

    private boolean verificarSobreposicao(List<Vigencia> vigenciaList, Vigencia vigencia) {
        for (Vigencia vige :
             vigenciaList) {
            if(vigencia.getInicioVigencia().isAfter(vige.getInicioVigencia()) && vigencia.getInicioVigencia().isBefore(vige.getFimVigencia())) {
                return true;
            }
            if(vigencia.getFimVigencia().isAfter(vige.getInicioVigencia()) && vigencia.getFimVigencia().isBefore(vige.getFimVigencia())) {
                return true;
            }
            if(vigencia.getInicioVigencia().equals(vige.getInicioVigencia()) || vigencia.getFimVigencia().equals(vige.getFimVigencia())) {
                return true;
            }
            if(vigencia.getInicioVigencia().equals(vige.getFimVigencia()) || vigencia.getFimVigencia().equals(vige.getInicioVigencia())) {
                return true;
            }
        }
        return false;
    }

    public Vigencia consultarVigenciaPorIdETarifaId(int vigeId, int tarifaId) {
        Optional<Vigencia> optionalVigencia = vigenciaRepository.findByIdAndTarifaId(vigeId, tarifaId);
        if(!optionalVigencia.isPresent()) {
            throw new VigenciaNotFoundException();
        }
        return optionalVigencia.get();
    }

    public Vigencia vencerVigencia(Vigencia vigencia, LocalDate fimVigencia) {
        LocalDate dataAtual = LocalDate.now();
        if(vigencia.getFimVigencia().isBefore(dataAtual)) {
            throw new FimDeVigenciaInvalidaException("Não é possível alterar vigências já vencidas");
        }
        if(fimVigencia.isBefore(vigencia.getInicioVigencia())) {
            throw new FimDeVigenciaInvalidaException("Fim de vigência não pode ser anterior a data de início");
        }
        if(fimVigencia.isBefore(dataAtual)) {
            throw new FimDeVigenciaInvalidaException("Fim de vigência não pode ser menor que a data atual");
        }
        if(fimVigencia.isAfter(vigencia.getFimVigencia())) {
            throw new FimDeVigenciaInvalidaException("Fim de vigência não pode ser estendida");
        }
        vigencia.setFimVigencia(fimVigencia);
        Vigencia vigenciaDB = vigenciaRepository.save(vigencia);
        System.out.println("Fim de vigência alterado com sucesso: " + vigenciaDB.toString());
        return vigenciaDB;
    }
}
