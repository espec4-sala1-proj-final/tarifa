package br.com.itau.tarifas.tarifa.dtos;

import br.com.itau.tarifas.tarifa.dtos.request.CriarVigenciaDTO;
import br.com.itau.tarifas.tarifa.models.Tarifa;
import br.com.itau.tarifas.tarifa.models.Vigencia;
import org.springframework.stereotype.Component;

@Component
public class VigenciaMapper {

    public Vigencia toVigencia(CriarVigenciaDTO criarVigenciaDTO, Tarifa tarifa) {
        Vigencia vigencia = new Vigencia();
        vigencia.setTarifa(tarifa);
        vigencia.setInicioVigencia(criarVigenciaDTO.getInicioVigencia());
        vigencia.setFimVigencia(criarVigenciaDTO.getFimVigencia());
        vigencia.setValorMinimo(criarVigenciaDTO.getValorMinimo());
        vigencia.setValorMaximo(criarVigenciaDTO.getValorMaximo());
        return vigencia;
    }
}
