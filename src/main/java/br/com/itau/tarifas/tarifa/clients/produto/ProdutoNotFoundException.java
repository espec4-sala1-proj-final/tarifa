package br.com.itau.tarifas.tarifa.clients.produto;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Produto não encontrado")
public class ProdutoNotFoundException extends RuntimeException {
}

