package br.com.itau.tarifas.tarifa.clients.contacontabil;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "contacontabil", configuration = ContaContabilClientConfiguration.class)
public interface ClientContaContabil {

    @GetMapping("/contabil/{id}")
    ContaContabil pesquisarPorId(@PathVariable int id);

}
