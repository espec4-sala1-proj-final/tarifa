package br.com.itau.tarifas.tarifa.controller;


import br.com.itau.tarifas.tarifa.dtos.TarifaMapper;
import br.com.itau.tarifas.tarifa.dtos.request.VencerVigenciaDTO;
import br.com.itau.tarifas.tarifa.dtos.request.TarifaRequest;
import br.com.itau.tarifas.tarifa.dtos.response.TarifaResponse;
import br.com.itau.tarifas.tarifa.dtos.VigenciaMapper;
import br.com.itau.tarifas.tarifa.dtos.request.CriarVigenciaDTO;
import br.com.itau.tarifas.tarifa.models.Tarifa;
import br.com.itau.tarifas.tarifa.models.Vigencia;
import br.com.itau.tarifas.tarifa.services.VigenciaService;
import br.com.itau.tarifas.tarifa.services.TarifaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/tarifas")
public class TarifaController {

	@Autowired
	private TarifaService tarifaService;

	@Autowired
	private TarifaMapper tarifaMapper;

	@Autowired
	private VigenciaMapper vigenciaMapper;

	@Autowired
	private VigenciaService vigenciaService;

    @GetMapping
    public Iterable<Tarifa> getTarifas() {
        return tarifaService.consultarTarifas();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TarifaResponse cadastrarTarifa(@RequestBody @Valid TarifaRequest tarifaRequest) {
        Tarifa tarifa = tarifaMapper.toTarifa(tarifaRequest);
        tarifa = tarifaService.cadastrar(tarifa);
        return tarifaMapper.toTarifaResponse(tarifa);
    }

    @GetMapping("/{id}")
    public Tarifa getTarifaPorId(@PathVariable(name = "id") int tarifaId) {
        return tarifaService.consultarTarifaPorId(tarifaId);
    }

    @GetMapping("/{id}/vigencias")
    public Iterable<Vigencia> consultarVigencias(@PathVariable(name = "id") int tarifaId) {
        return vigenciaService.consultarVigenciasPorTarifaId(tarifaId);
    }

    @PostMapping("/{id}/vigencias")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Vigencia criarVigencia(@RequestBody @Valid CriarVigenciaDTO criarVigenciaDTO, @PathVariable(name = "id") int tarifaId) {
        Tarifa tarifa = tarifaService.consultarTarifaPorId(tarifaId);
        Vigencia vigencia = vigenciaMapper.toVigencia(criarVigenciaDTO, tarifa);
        return vigenciaService.criarVigencia(vigencia);
    }

    @PatchMapping("/{id}/vigencias/{vigeId}")
    public Vigencia vencerVigencia(@RequestBody @Valid VencerVigenciaDTO vencerVigenciaDTO, @PathVariable(name = "id") int id, @PathVariable(name = "vigeId") int vigeId) {
        Tarifa tarifa = tarifaService.consultarTarifaPorId(id);
        Vigencia vigencia = vigenciaService.consultarVigenciaPorIdETarifaId(vigeId, id);
        return vigenciaService.vencerVigencia(vigencia, vencerVigenciaDTO.getFimVigencia());
    }

}
