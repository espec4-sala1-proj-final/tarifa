package br.com.itau.tarifas.tarifa.repositories;

import br.com.itau.tarifas.tarifa.models.Tarifa;
import org.springframework.data.repository.CrudRepository;

public interface TarifaRepository extends CrudRepository<Tarifa, Integer> {

}

