package br.com.itau.tarifas.tarifa.services;

import br.com.itau.tarifas.tarifa.clients.contacontabil.ClientContaContabil;
import br.com.itau.tarifas.tarifa.clients.contacontabil.ContaContabil;
import br.com.itau.tarifas.tarifa.clients.produto.ClientProduto;
import br.com.itau.tarifas.tarifa.clients.produto.Produto;
import br.com.itau.tarifas.tarifa.exceptions.TarifaNotFoundException;
import br.com.itau.tarifas.tarifa.models.Tarifa;
import br.com.itau.tarifas.tarifa.repositories.TarifaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class TarifaService {

	@Autowired
	TarifaRepository tarifaRepository;

	@Autowired
	ClientProduto clientProduto;

	@Autowired
	ClientContaContabil clientContaContabil;

	public Tarifa cadastrar(Tarifa tarifa) {
		Produto produto;
		System.out.println("Chamando o microserviço de Produto: "+"produtoID: "+ tarifa.getProdutoId());
		produto = clientProduto.buscarPorId(tarifa.getProdutoId());
		tarifa.setProdutoId(produto.getId());

		ContaContabil contaContabil;
		System.out.println("Chamando o microserviço de ContaContabil: "+"contaContabilID: "+ tarifa.getContaContabilId());
		contaContabil = clientContaContabil.pesquisarPorId(tarifa.getContaContabilId());
		tarifa.setContaContabilId(contaContabil.getId());

		tarifa = tarifaRepository.save(tarifa);
		System.out.println("Tarifa incluída com sucesso: " + tarifa.toString());

		return tarifa;
	}

	public Iterable<Tarifa> consultarTarifas() {
		return tarifaRepository.findAll();
	}

	public Tarifa consultarTarifaPorId(int tarifaId) {
		Optional<Tarifa> optionalTarifa = tarifaRepository.findById(tarifaId);
		if(!optionalTarifa.isPresent()) {
			throw new TarifaNotFoundException();
		}
		return optionalTarifa.get();
	}

}