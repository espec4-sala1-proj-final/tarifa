package br.com.itau.tarifas.tarifa.dtos.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TarifaRequest {

    @NotBlank
    @Size(min = 3)
    @NotNull(message = "Campo nome não pode ser nulo")
    private String nome;

    @NotNull(message = "Campo produtoId não pode ser nulo")
    private Integer produtoId;

    @NotNull(message = "Campo conta não pode ser nulo")
    private Integer contaContabilId;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(Integer produtoId) {
        this.produtoId = produtoId;
    }

    public Integer getContaContabilId() {
        return contaContabilId;
    }

    public void setContaContabilId(Integer contaContabilId) {
        this.contaContabilId = contaContabilId;
    }
}