package br.com.itau.tarifas.tarifa.dtos.request;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class VencerVigenciaDTO {

    @NotNull(message = "Data fim de vigência não pode ser nula")
    private LocalDate fimVigencia;

    public LocalDate getFimVigencia() {
        return fimVigencia;
    }

    public void setFimVigencia(LocalDate fimVigencia) {
        this.fimVigencia = fimVigencia;
    }
}
