package br.com.itau.tarifas.tarifa.clients.produto;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "produto", configuration = ProdutoClientConfiguration.class)
public interface ClientProduto {

    @GetMapping("/produtos/{Id}")
    Produto buscarPorId(@PathVariable Integer Id);

}
