package br.com.itau.tarifas.tarifa.clients.produto;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ProdutoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ProdutoNotFoundException();
        }
        return errorDecoder.decode(s, response);

    }
}
